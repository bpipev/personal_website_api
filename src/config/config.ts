import { ConnectionConfig } from 'mysql';
import { readFileSync, existsSync } from 'fs';

export interface Config {
    PLAYGROUND_HOST: string;
    PLAYGROUND_USER: string;
    PLAYGROUND_PASS: string;
    PLAYGROUND_DB: string;
    FACEBOOK_ACCESS_TOKEN_PROD: string;
    FACEBOOK_ACCESS_TOKEN_DEV: string;
}

export class Configuration {
    private cfg: Config;
    private static defaultConfig: Configuration;
    issuer = 'Playground';
    audience = 'https://bpipev.herokuapp.com';
    expiresIn = '12h';
    algorithm = 'RS256';
    publicKey = existsSync('./cert/jwt/public.key')
        ? readFileSync('./cert/jwt/public.key', 'utf8')
        : process.env.JWT_PUBLIC_KEY;
    privateKey = existsSync('./cert/jwt/private.key')
        ? readFileSync('./cert/jwt/private.key', 'utf8')
        : process.env.JWT_PRIVATE_KEY;
    constructor(path: string) {
        if (existsSync(path)) {
            this.cfg = JSON.parse(readFileSync(path).toString());
        }
    }

    static getDefaultConfig() {
        if (this.defaultConfig == null) {
            return new Configuration('./config.json');
        }
        return this.defaultConfig;
    }

    getDatabaseDevConfig() {
        return <ConnectionConfig>{
            host: process.env.PLAYGROUND_DEV_HOST,
            user: process.env.PLAYGROUND_DEV_USER,
            password: process.env.PLAYGROUND_DEV_PASS,
            database: process.env.PLAYGROUND_DEV_DB
        };
    }

    getDatabaseProdConfig() {
        return <ConnectionConfig>{
            host: process.env.PLAYGROUND_PROD_HOST || this.cfg.PLAYGROUND_HOST,
            user: process.env.PLAYGROUND_PROD_USER || this.cfg.PLAYGROUND_USER,
            password:
                process.env.PLAYGROUND_PROD_PASS || this.cfg.PLAYGROUND_PASS,
            database: process.env.PLAYGROUND_PROD_DB || this.cfg.PLAYGROUND_DB,
            ssl: {
                ca:
                    process.env.PLAYGROUND_PROD_SERVER_CA ||
                    readFileSync('./cert/server-ca.pem'),
                key:
                    process.env.PLAYGROUND_PROD_CLIENT_KEY ||
                    readFileSync('./cert/client-key.pem'),
                cert:
                    process.env.PLAYGROUND_PROD_CLIENT_CERT ||
                    readFileSync('./cert/client-cert.pem')
            }
        };
    }

    getDatabaseTestConfig() {
        return <ConnectionConfig>{
            host: this.cfg.PLAYGROUND_HOST,
            user: this.cfg.PLAYGROUND_USER,
            password: this.cfg.PLAYGROUND_PASS,
            // database: this.cfg.PLAYGROUND_DB Don't specify inital db since it may not exist
        };
    }

    getDatabaseConfig() {
        const dev = this.getDatabaseDevConfig();
        const prod = this.getDatabaseProdConfig();
        return process.env.NODE_ENV === 'development' ? dev : prod;
    }

    getFbAccessToken() {
        return process.env.NODE_ENV === 'development'
            ? this.cfg.FACEBOOK_ACCESS_TOKEN_DEV
            : process.env.FACEBOOK_ACCESS_TOKEN_PROD;
    }
}
