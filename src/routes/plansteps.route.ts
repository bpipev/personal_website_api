import { Router, Request, Response, NextFunction } from 'express';
import { deleteStepsController, insertStepsController } from '../controllers/plansteps.controller';

const planStepsRoute = Router();

planStepsRoute.post('/', insertStepsController);

planStepsRoute.delete('/:id', deleteStepsController);

export { planStepsRoute };
