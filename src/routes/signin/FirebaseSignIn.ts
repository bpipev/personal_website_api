import { Router, Request, Response, NextFunction } from 'express';
import * as admin from 'firebase-admin';
import { readFileSync } from 'fs';
import { getAuthorization } from './GoogleSignIn';
import * as jwt from 'jsonwebtoken';
import { Database } from '../../models/Database';
import { Configuration } from '../../config/config';
import { User } from '../../models/User';

/**
 * When authorizing via a service account, you have
 * two choices for providing the credentials to your
 * application. You can either set the GOOGLE_APPLICATION_CREDENTIALS
 * environment variable, or you can explicitly pass the path to the
 * service account key in code. The first option is more secure and
 * is strongly recommended.
 * https://firebase.google.com/docs/admin/setup/
 */

var serviceAccount = JSON.parse(
    readFileSync(
        './cert/my-flutter-app-3b318-firebase-adminsdk-3we6s-6781debeab.jsosn'
    ).toString()
);
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://my-flutter-app-3b318.firebaseio.com'
});

const firebaseAuth = Router();
firebaseAuth.all('/', firebaseAuthenticate);
async function firebaseAuthenticate(
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const config = Configuration.getDefaultConfig();
        const signOptions = {
            issuer: config.issuer,
            audience: config.audience,
            expiresIn: config.expiresIn,
            algorithm: config.algorithm
        } as jwt.SignOptions;
        const auth = req.headers['authorization'];
        if (auth == null) {
            throw new Error('Invalid Authorization.');
        }
        const token = getAuthorization(auth);
        // idToken comes from the client app
        let decodedToken = await admin.auth().verifyIdToken(token);
        let user = <User>{ login_id: decodedToken.uid, login_type: 'firebase' };
        const result = await Database.getPlayground(
            config.getDatabaseConfig()
        ).getInsertUser(user);
        signOptions.subject = String(result.user_id);
        const newToken = jwt.sign({}, config.privateKey, signOptions);
        res.status(200).send({ token: newToken });
    } catch (err) {
        next(err);
    }
}

export { firebaseAuth };
