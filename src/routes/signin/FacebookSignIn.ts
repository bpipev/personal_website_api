import { Router, Request, Response, NextFunction } from 'express';
import * as Bluebird from 'bluebird';
import { IncomingMessage } from 'http';
import { FacebookResponse } from '../../models/Facebook';
import * as jwt from 'jsonwebtoken';
import { Configuration } from '../../config/config';
import { User } from '../../models/User';
import { Database } from '../../models/Database';
const rq = Bluebird.promisify(require('request'), { multiArgs: true });
Bluebird.promisifyAll(rq, { multiArgs: true });

const facebookAuth = Router();

facebookAuth.all('/', fbAuth);

async function fbAuth(req: Request, res: Response, next: NextFunction) {
    try {
        const config = Configuration.getDefaultConfig();
        const signOptions = {
            issuer: config.issuer,
            audience: config.audience,
            expiresIn: config.expiresIn,
            algorithm: config.algorithm
        } as jwt.SignOptions;
        const token = req.headers['authorization'];
        const path = `https://graph.facebook.com/debug_token?input_token=${token}&access_token=${config.getFbAccessToken()}`;
        let response: IncomingMessage;
        let body: FacebookResponse;
        [response, body] = await (rq as any)(path, { json: true });
        if (body.data != undefined) {
            const result = await Database.getPlayground(
                config.getDatabaseConfig()
            ).getInsertUser(<User>{
                login_id: body.data.user_id,
                login_type: 'facebook'
            });
            signOptions.subject = String(result.user_id); // TODO: in next() ?? middleware
            const newToken = jwt.sign({}, config.privateKey, signOptions);
            res.status(200).send({ token: newToken });
        } else {
            throw new Error('Invalid Authorization.');
        }
    } catch (err) {
        next(err);
    }
}

export { facebookAuth };
