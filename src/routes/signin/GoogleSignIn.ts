import { Router, Request, Response, NextFunction } from 'express';
import { OAuth2Client } from 'google-auth-library';
import { User } from '../../models/User';
import { Database } from '../../models/Database';
import * as jwt from 'jsonwebtoken';
import { Configuration } from '../../config/config';

const googleAuth = Router();
const CLIENT_ID =
    '367118618715-ee8gk239gvh7vtpt6mgski3c11h956ih.apps.googleusercontent.com';
const client = new OAuth2Client(CLIENT_ID);
googleAuth.all('/', googAuth);
async function googAuth(req: Request, res: Response, next: NextFunction) {
    try {
        const config = Configuration.getDefaultConfig();
        const signOptions = {
            issuer: config.issuer,
            audience: config.audience,
            expiresIn: config.expiresIn,
            algorithm: config.algorithm
        } as jwt.SignOptions;
        const auth = req.headers['authorization'];
        if (auth == null) {
            throw new Error('Invalid Authorization.');
        }
        const token = getAuthorization(auth);
        const user = await verify(token);
        const result = await Database.getPlayground(
            config.getDatabaseConfig()
        ).getInsertUser(user);
        signOptions.subject = String(result.user_id);
        const newToken = jwt.sign({}, config.privateKey, signOptions);
        res.status(200).send({ token: newToken });
    } catch (err) {
        next(err);
    }
}

function getAuthorization(token: string): string {
    let sanitizedToken = token.trim();
    sanitizedToken = sanitizedToken.substr(7);
    return sanitizedToken;
}

async function verify(token: string): Promise<User> {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: CLIENT_ID
    });
    const payload = ticket.getPayload();
    const userid = payload.sub;
    const id = payload.aud;
    if (id !== CLIENT_ID) {
        throw new Error('API not called with correct client id.');
    }
    if (
        ['accounts.google.com', 'https://accounts.google.com'].indexOf(
            payload.iss
        ) == -1
    ) {
        throw new Error('Wrong iss.');
    }
    return <User>{
        login_id: userid,
        login_type: 'google'
    };
}

export { googleAuth, getAuthorization };
