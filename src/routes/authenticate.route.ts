import { Router } from 'express';
import { authenticateController } from '../controllers/authenticate.controller';

const authenticate = Router();

authenticate.all('/', authenticateController);

export { authenticate };
