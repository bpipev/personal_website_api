import { Router } from 'express';
import { getDashboardPlansController, getPublicPlansController } from '../controllers/publicdata.controller';

const publicDataRoute = Router();

publicDataRoute.get('/', getDashboardPlansController);
publicDataRoute.get('/plan/:id', getPublicPlansController);

export { publicDataRoute };
