import { Router } from 'express';
import { deletePlanController, getAllUserPlansController, getPlanWithStepsController, insertPlanController, makePlanPrivateController, makePlanPublicController, updatePlanController } from '../controllers/plans.controller';

const plansRoute = Router();

plansRoute.get('/:id', getPlanWithStepsController);

plansRoute.get('/makepublic/:id', makePlanPublicController);

plansRoute.get('/makeprivate/:id', makePlanPrivateController);

plansRoute.get('/', getAllUserPlansController);

plansRoute.delete('/:id', deletePlanController);

plansRoute.put('/:id', updatePlanController);

plansRoute.post('/', insertPlanController);

export { plansRoute };
