import { Router } from 'express';

import { facebookAuth } from './signin/FacebookSignIn';
import { googleAuth } from './signin/GoogleSignIn';
import { firebaseAuth } from './signin/FirebaseSignIn';

const signinRoute = Router();

signinRoute.use('/google', googleAuth);
signinRoute.use('/facebook', facebookAuth);
// signin.use('/firebase', firebaseAuth);

export { signinRoute };
