import { Router } from 'express';
import {
    addPersonController, addScheduleController, addWorkPreferenceController, deletePersonController, deleteScheduleController, deleteWorkPreferenceController, getPeopleNotInScheduleController, getPersonController,
    getScheduleController, getScheduleListController, removeScheduledPersonController, updateDayValueController
} from '../controllers/schedules.controller';

const schedulesRoute = Router();

// TODO: Should /person be somewhere else?
schedulesRoute.post('/person', addPersonController);
schedulesRoute.get('/person', getPeopleNotInScheduleController);
schedulesRoute.get('/person/:id', getPersonController);
schedulesRoute.post('/person/:id', addWorkPreferenceController);
schedulesRoute.delete('/work_preference/:id', deleteWorkPreferenceController);
schedulesRoute.get('/:id', getScheduleController);
schedulesRoute.get('/', getScheduleListController);
schedulesRoute.post('/', addScheduleController);
schedulesRoute.put('/', updateDayValueController);
schedulesRoute.delete('/', deletePersonController);
schedulesRoute.delete('/person_schedule', removeScheduledPersonController);
schedulesRoute.delete('/:id', deleteScheduleController);

export { schedulesRoute };
