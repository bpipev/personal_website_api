import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import { signinRoute } from './routes/signIn.route';
import { plansRoute } from './routes/plans.route';
import { planStepsRoute } from './routes/plansteps.route';
import { Response, Request, NextFunction } from 'express';
import { authenticate } from './routes/authenticate.route';
import { publicDataRoute } from './routes/publicdata.route';
import { schedulesRoute } from './routes/schedules.route';

const app = express();

app.use(logger('dev')); // TODO: prod?
app.use(bodyParser.json());
app.use(function(req: Request, res: Response, next: NextFunction) {
    if (process.env.NODE_ENV != 'development') {
        res.header(
            'Access-Control-Allow-Origin',
            'https://bpipev.herokuapp.com'
        );
    } else {
        res.header('Access-Control-Allow-Origin', '*');
    }
    res.header(
        'Access-Control-Allow-Headers',
        'Authorization, Origin, X-Requested-With, Content-Type, Accept'
    );
    res.header(
        'Access-Control-Allow-Methods',
        'GET, POST, OPTIONS, PUT, DELETE'
    );
    next();
});
app.options('*', function(req: Request, res: Response, next: NextFunction) {
    res.sendStatus(200);
});
app.use('/api/v1/public', publicDataRoute);
app.use('/api/v1/schedules', schedulesRoute);

app.use('/api/v1/login', signinRoute);
app.use('*', authenticate);
app.use('/api/v1/plans', plansRoute);
app.use('/api/v1/steps', planStepsRoute); // TODO: put planSteps as part of plans /plans/:id/steps

app.use(function(
    error: Error,
    req: Request,
    res: Response,
    next: NextFunction
) {
    res.status(500).send({ message: error.message }); // TODO: Have different error types and check them
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => console.log(`App listening on port ${PORT}`));
