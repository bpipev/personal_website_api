import { Request, Response, NextFunction } from 'express';
import { Person } from '../models/Schedule';
import { WorkPreference } from '../models/WorkPreference';
import { ScheduleService } from '../services/schedule.service';

const service = new ScheduleService();

export async function getPersonController(req: Request, res: Response, next: NextFunction) {
    try {
        const person_id = Number(req.params.id);
        const person = await service.getPerson(person_id);
        const work_preferences = await service.getWorkPreferences(person_id);
        const result = { name: person.name, work_preferences: work_preferences };
        res.status(200).send(result);
    } catch (err) {
        next(err);
    }
}

export async function deleteWorkPreferenceController(req: Request, res: Response, next: NextFunction) {
    try {
        const preference_id = Number(req.params.id);
        const result = await service.deleteWorkPreference(preference_id);
        res.status(200).send(result);
    } catch (err) {
        next(err);
    }
}

export async function getPeopleNotInScheduleController(req: Request, res: Response, next: NextFunction) {
    try {
        const schedule_id = Number(req.query.schedule_id);
        const people = await service.getPeopleNotInSchedule(schedule_id);
        res.status(200).send(people);
    } catch (err) {
        next(err);
    }
}

export async function getScheduleController(req: Request, res: Response, next: NextFunction) {
    try {
        const scheduleId = Number(req.params.id);
        const schedule = await service.getSchedule(scheduleId);
        const schedule_response = await service.addWorkPreferencesToSchedule(schedule);
        res.status(200).send(schedule_response);
    } catch (err) {
        next(err);
    }
}

export async function deleteScheduleController(req: Request, res: Response, next: NextFunction) {
    try {
        const scheduleId = Number(req.params.id);
        const response = await service.deleteSchedule(scheduleId);
        res.status(200).send(response);
    } catch (err) {
        next(err);
    }
}

export async function getScheduleListController(req: Request, res: Response, next: NextFunction) {
    try {
        const scheduleList = await service.getScheduleList(1); // TODO: pass dept id
        res.status(200).send(scheduleList);
    } catch (err) {
        next(err);
    }
}

export async function addScheduleController(req: Request, res: Response, next: NextFunction) {
    try {
        const body = req.body;
        const schedule_id = await service.addSchedule(body.start, body.duration, 1, body.add_people_from); // TODO: pass dept id
        const schedule = await service.getSchedule(schedule_id);
        res.status(200).send(schedule);
    } catch (err) {
        next(err);
    }
}

export async function addWorkPreferenceController(req: Request, res: Response, next: NextFunction) {
    try {
        const body = <WorkPreference>req.body;
        const schedule = await service.addWorkPreference(body);
        res.status(200).send(schedule);
    } catch (err) {
        next(err);
    }
}

export async function addPersonController(req: Request, res: Response, next: NextFunction) {
    try {
        const person = <Person>req.body;
        const schedule_id = await service.addPerson(person);
        const schedule = await service.getSchedule(schedule_id);
        const schedule_response = await service.addWorkPreferencesToSchedule(schedule);
        res.status(200).send(schedule_response);
    } catch (err) {
        next(err);
    }
}

export async function updateDayValueController(req: Request, res: Response, next: NextFunction) {
    try {
        const value = req.body;
        const date = value.date;
        const id = value.id;
        const newValue = value.new;
        const schedule_id = value.schedule_id;
        const response = await service.updateDayValue(date, id, newValue, schedule_id);
        res.status(200).send(response);
    } catch (err) {
        next(err);
    }
}

export async function deletePersonController(req: Request, res: Response, next: NextFunction) {
    try {
        const id = Number(req.query.id);
        const result = await service.deletePerson(id);
        res.status(200).send(result);
    } catch (err) {
        next(err);
    }
}

export async function removeScheduledPersonController(req: Request, res: Response, next: NextFunction) {
    try {
        const schedule_id = Number(req.query.schedule_id);
        const person_id = Number(req.query.person_id);
        const result = await service.removeScheduledPerson(schedule_id, person_id);
        res.status(200).send(result);
    } catch (err) {
        next(err);
    }
}
