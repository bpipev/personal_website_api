import { Request, Response, NextFunction } from 'express';
import { Plan } from '../models/Plan';
import { PlansService } from '../services/plans.service';

const service = new PlansService();

// TODO: https://medium.com/@Abazhenov/using-async-await-in-express-with-node-8-b8af872c0016
export const getPlanWithStepsController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user_id = res.locals.user.user_id;
        const id = Number(req.params.id);
        const result = await service.getPlansWithSteps(user_id, id);
        res.status(200).send(result);
    } catch (err) {
        next(err);
    }
}

export const makePlanPublicController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user_id = res.locals.user.user_id;
        const id = Number(req.params.id);
        const result = await service.makePlanPublic(user_id, id);
        res.status(200).send({ success: result });
    } catch (err) {
        next(err);
    }
}

// TODO: /:id/makeprivate and put in another module together /:id
export const makePlanPrivateController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user_id = res.locals.user.user_id;
        const id = Number(req.params.id);
        const result = await service.makePlanPrivate(user_id, id);
        res.status(200).send({ success: result });
    } catch (err) {
        next(err);
    }
}

export const getAllUserPlansController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user_id = res.locals.user.user_id;
        const plans = await service.getAllUserPlans(user_id);
        res.send(plans);
    } catch (err) {
        next(err);
    }
}

export const deletePlanController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user_id = res.locals.user.user_id;
        const plan_id = Number(req.params.id);
        const result = await service.deletePlan(user_id, plan_id);
        res.status(200).send({});
    } catch (err) {
        next(err);
    }
}

export const updatePlanController = async (req: Request, res: Response, next: NextFunction) => {
    // TODO: update plan
    res.sendStatus(404);
}

export const insertPlanController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const plan = <Plan>req.body;
        plan.user_id = res.locals.user.user_id;
        await service.insertPlan(plan);
        res.status(200).send(plan);
    } catch (err) {
        next(err);
    }
}