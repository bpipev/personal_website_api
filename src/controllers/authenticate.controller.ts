import { Request, Response, NextFunction } from 'express';
import { AuthenticationService } from '../services/authenticate.service';

const service = new AuthenticationService();

export const authenticateController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const auth = req.headers['authorization'];
        let result = await service.authenticate(auth);
        res.locals.user = result;
    } catch (err) {
        next(err);
    }

    next();
}

