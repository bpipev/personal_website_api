import { Request, Response, NextFunction } from 'express';
import { Dashboard } from '../models/Dashboard';
import { PublicDataService } from '../services/publicdata.service';

const service = new PublicDataService();

export async function getDashboardPlansController(req: Request, res: Response, next: NextFunction) {
    try {
        const plans = await service.getDashboardPlans();
        res.status(200).send(<Dashboard>{ plans: plans });
    } catch (err) {
        next(err);
    }
}

export async function getPublicPlansController(req: Request, res: Response, next: NextFunction) {
    try {
        const id = Number(req.params.id);
        const result = await service.getPublicPlans(id);
        res.status(200).send(result);
    } catch (err) {
        next(err);
    }
}
