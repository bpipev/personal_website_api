import { Request, Response, NextFunction } from 'express';
import { PlanStep } from '../models/PlanStep';
import { PlanStepsService } from '../services/plansteps.service';

const service = new PlanStepsService();

// TODO: https://medium.com/@Abazhenov/using-async-await-in-express-with-node-8-b8af872c0016
export const insertStepsController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user_id = res.locals.user.user_id;
        const planStep = <PlanStep>req.body;
        await service.insertSteps(user_id, planStep);
        res.status(200).send(planStep);
    } catch (error) {
        next(error);
    }
}

export const deleteStepsController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user_id = res.locals.user.user_id;
        const stepId = Number(req.params.id);
        await service.deleteSteps(user_id, stepId);
        res.status(200).send({});
    } catch (error) {
        next(error);
    }
}




