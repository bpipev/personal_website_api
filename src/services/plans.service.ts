import { Database } from '../models/Database';
import { Configuration } from '../config/config';
import { Plan } from '../models/Plan';
import { PlaygroundDB } from '../db/PlaygroundDB';

class PlansService {
    db: PlaygroundDB;
    constructor(db: PlaygroundDB = null) {
        if (!db) {
            const config = Configuration.getDefaultConfig();
            this.db = Database.getPlayground(config.getDatabaseConfig());
        } else {
            this.db = db;
        }
    }
    async getPlansWithSteps(user_id: number, id: number): Promise<Plan> {
        return await this.db.getPlanWithSteps(user_id, id);
    }

    async makePlanPublic(user_id: number, id: number) {
        return await this.db.makePlanPublic(user_id, id);
    }

    async makePlanPrivate(user_id: number, id: number) {
        return await this.db.makePlanPrivate(user_id, id);
    }

    async deletePlan(user_id: number, plan_id: number) {
        return await this.db.deletePlan(user_id, plan_id);
    }

    async getAllUserPlans(user_id: number) {
        return await this.db.getAllUserPlans(user_id);
    }

    async insertPlan(plan: Plan) {
        return await this.db.insertPlan(plan);
    }
}
export { PlansService }