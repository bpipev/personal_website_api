import { Database } from '../models/Database';
import { Configuration } from '../config/config';
import { PlaygroundDB } from '../db/PlaygroundDB';

class PublicDataService {
    db: PlaygroundDB;
    constructor(db: PlaygroundDB = null) {
        if (!db) {
            const config = Configuration.getDefaultConfig();
            this.db = Database.getPlayground(config.getDatabaseConfig());
        } else {
            this.db = db;
        }
    }
    async getDashboardPlans() {
        return await this.db.getDashboardPlans();
    }

    async getPublicPlans(id: number) {
        return await this.db.getPublicPlans(id);
    }
}
export { PublicDataService }