import { Database } from '../models/Database';
import { Configuration } from '../config/config';
import { DayScheduleType, Person, Schedule, ScheduleLength } from '../models/Schedule';
import { DateTime } from 'luxon';
import { WorkPreference } from '../models/WorkPreference';
import { ScheduleDB } from '../db/ScheduleDB';

class ScheduleService {
    db: ScheduleDB;
    constructor(db: ScheduleDB = null) {
        if (!db) {
            const config = Configuration.getDefaultConfig();
            const dbconfig = config.getDatabaseConfig();
            dbconfig.database = 'scheduledb';
            this.db = Database.getScheduleDB(dbconfig);
        } else {
            this.db = db;
        }
    }

    async getPeopleNotInSchedule(schedule_id: number) {
        return (await this.db.getPeopleNotInSchedule(schedule_id)).map((x: any) => { return { id: x.person_id, name: x.name }; });
    }

    async getPerson(person_id: number) {
        return { name: (await this.db.getPerson(person_id)).name };
    }

    async addWorkPreference(work_preference: WorkPreference) {
        return { id: await this.db.addWorkPreference(work_preference) };
    }

    async getWorkPreferences(person_id: number): Promise<WorkPreference[]> {
        const connection = await this.db.getConnection();
        const result = await this.db.getWorkPreferences(person_id, connection);
        connection.release();
        return <WorkPreference[]>result;
    }

    async addWorkPreferencesToSchedule(schedule: Schedule) {
        const connection = await this.db.getConnection();
        if (schedule) {
            for (let person_id of Object.keys(schedule.data)) {
                const work_preferences = await this.db.getWorkPreferences(Number(person_id), connection);
                schedule.data[person_id].work_preferences = work_preferences;
            };
        }
        connection.release();
        return schedule;
    }

    async deleteWorkPreference(preference_id: number) {
        return await this.db.deleteWorkPreference(preference_id);
    }

    async addSchedule(start: number, duration: number, department_id: number, add_people_from: number) {
        const startDate = DateTime.fromSeconds(start).toFormat('yyyy-LL-dd');
        const schedule_id = await this.db.addSchedule(startDate, duration, department_id, false);
        if (add_people_from) {
            const otherSchedule = await this.getSchedule(add_people_from);
            for (const key of Object.keys(otherSchedule.data)) {
                const person_id = Number(key);
                // TODO: fix schedule type
                await this.addPerson(<Person>{
                    schedule_id: schedule_id,
                    schedule_type: ScheduleLength.HOURS_8,
                    work_week_hours: 40,
                    id: person_id
                });
            };
        }
        return schedule_id;
    }

    async getScheduleList(department_id: number) {
        const connection = await this.db.getConnection();
        const scheduleList = await this.db.getScheduleList(department_id, connection);
        const result = scheduleList.map(schedule => {
            return {
                is_current_schedule: Boolean(schedule.is_current_schedule),
                schedule_id: schedule.schedule_id,
                start: schedule.start,
                duration: schedule.duration,
                department_id: schedule.department_id,
                department_name: schedule.name
            };
        })
        connection.release();
        return result;
    }

    async getSchedule(schedule_id: number) {
        const dbResponse = await this.db.getSchedule(schedule_id);
        let schedule: Schedule;
        dbResponse.forEach(value => {
            const currentDay = value.day;
            if (!schedule) {
                const data: any = {};
                const dates = new Array<number>();
                for (let i = 0; i < value.duration; i++) {
                    dates.push(DateTime.fromSeconds(value.start).plus({ days: i }).toSeconds());
                }
                const date: any = {};
                if (value.person_id) {
                    date[currentDay] = value.day_schedule_type;
                    data[value.person_id] = { workdays: date, name: value.name };
                }
                schedule = <Schedule>{ currentUser: -1, data: data, dates: dates, id: schedule_id };
            }
            else {
                if (!schedule.data[value.person_id]) {
                    const date: any = {};
                    date[currentDay] = value.day_schedule_type;
                    schedule.data[value.person_id] = { workdays: date, name: value.name };
                }
                else {
                    schedule.data[value.person_id].workdays[currentDay] = value.day_schedule_type;
                }
            }
        });
        return schedule;
    }

    async deleteSchedule(schedule_id: number) {
        return await this.db.deleteSchedule(schedule_id);
    }

    async updateDayValue(date: string, person_id: number, newValue: DayScheduleType, schedule_id: number) {
        const connection = await this.db.getConnection();
        const exists = await this.db.getDayExists(date, person_id, connection);

        let response = { status: 'ERROR' }
        if (exists) {
            response = await this.db.updateDayValue(newValue, person_id, date, connection);
        } else {
            const scheduled_id = await this.db.getScheduledPeopleId(schedule_id, person_id, connection);
            const unixDate = DateTime.fromFormat(date, 'yyyy-LL-dd').toSeconds();
            const result = await this.db.insertDays([[newValue, unixDate]], scheduled_id, connection);
            if (result.affectedRows === 1) {
                response.status = 'OK';
            }
        }
        connection.release();
        return response;
    }

    async addPerson(person: Person): Promise<number> {
        const connection = await this.db.getConnection();
        let workPreferences: any[] = [];
        if (person.id) {
            workPreferences = await this.db.getWorkPreferences(person.id, connection);
        } else {
            person.id = await this.db.insertPerson(person.name, connection);
        }
        const schedule = await this.db.getScheduleInfo(person.schedule_id, connection);

        if (schedule) {
            let wkndBeforeScheduleDate = DateTime.fromSeconds(schedule.startDate);
            if (wkndBeforeScheduleDate.weekday < 7) {
                wkndBeforeScheduleDate = wkndBeforeScheduleDate.minus({ days: wkndBeforeScheduleDate.weekday });
            }
            else {
                wkndBeforeScheduleDate = wkndBeforeScheduleDate.minus({ days: 1 });
            }
            const wkndBeforeScheduleDateStr = wkndBeforeScheduleDate.toFormat('yyyy-LL-dd');
            const weekendBeforeSchedule = await this.db.getWeekendBeforeSchedule(wkndBeforeScheduleDateStr, connection);
            const existingPeopleDbResponse = await this.db.getExistingPeople(person.schedule_id, connection);

            let daysToInsert = new Array<[string, number]>();
            // TODO: refactor existingPeopleDbResponse
            existingPeopleDbResponse.forEach((x: any) => {
                const unix_day = DateTime.fromJSDate(x.day).toSeconds();
                schedule.addExistingScheduledDay(x.name, unix_day, x.day_schedule_type, x.person_id);
            });
            daysToInsert = schedule.addPerson(person, weekendBeforeSchedule, workPreferences);
            await this.db.schedulePerson(person.id, person.schedule_id, connection);
            const scheduled_id = await this.db.getScheduledPeopleId(person.schedule_id, person.id, connection);
            await this.db.insertDays(daysToInsert, scheduled_id, connection);
        }
        connection.release();
        return person.schedule_id;
    }

    async deletePerson(id: number) {
        return await this.db.deletePerson(id);
    }

    async removeScheduledPerson(schedule_id: number, person_id: number) {
        return await this.db.removeScheduledPerson(schedule_id, person_id);
    }
}
export { ScheduleService }