import { PlanStep } from '../models/PlanStep';
import { Database } from '../models/Database';
import { Configuration } from '../config/config';
import { PlaygroundDB } from '../db/PlaygroundDB';

class PlanStepsService {
    db: PlaygroundDB;
    constructor(db: PlaygroundDB = null) {
        if (!db) {
            const config = Configuration.getDefaultConfig();
            this.db = Database.getPlayground(config.getDatabaseConfig());
        } else {
            this.db = db;
        }
    }
    async insertSteps(user_id: number, planStep: PlanStep) {
        await this.db.insertSteps(user_id, planStep.plan_id, [planStep]);
    }

    async deleteSteps(user_id: number, stepId: number) {
        await this.db.deleteStep(user_id, stepId);
    }
}
export { PlanStepsService }