import * as jwt from 'jsonwebtoken';
import { Database } from '../models/Database';
import { User } from '../models/User';
import { jwtPayload } from '../models/JwtPayload';
import { Configuration } from '../config/config';

class AuthenticationService {
    async authenticate(auth: string) {
        const config = Configuration.getDefaultConfig();
        const signOptions = {
            issuer: config.issuer,
            audience: config.audience
        } as jwt.SignOptions;
        if (auth == null) {
            throw new Error('Invalid Authorization.');
        }
        const token = this.getAuthorization(auth);
        const decoded = jwt.verify(
            token,
            config.publicKey,
            signOptions
        ) as jwtPayload;

        if (decoded.sub === 'undefined') {
            throw new Error("Decoded.sub is undefined");
        }

        let result = await Database.getPlayground(
            config.getDatabaseConfig()
        ).getUserById({
            user_id: Number(decoded.sub)
        } as User);
        return result;
    }

    private getAuthorization(token: string): string {
        let sanitizedToken = token.trim();
        sanitizedToken = sanitizedToken.substr(7);
        return sanitizedToken;
    }
}
export { AuthenticationService }