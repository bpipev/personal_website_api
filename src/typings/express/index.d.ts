import { Response } from 'express';
import { User } from '../../models/User';

declare module 'express' {
    export interface Response {
        locals: Locals;
    }
}

interface Locals {
    user: User;
}
