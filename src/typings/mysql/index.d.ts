import { Pool, Query } from 'mysql';

declare module 'mysql' {
    export interface Pool {
        getConnectionAsync(): Promise<PoolConnection>;
    }
    export interface PoolConnection {
        queryAsync(query: Query): Promise<any>;
    }
}