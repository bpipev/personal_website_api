import {
    PoolConnection,
    Query
} from 'mysql';
import { Schedule, ScheduleDBResponse, DayScheduleType } from '../models/Schedule';
import * as moment from 'moment';
import { DB } from './DB';
import { WorkPreference } from '../models/WorkPreference';
import { DateTime } from 'luxon';

export class ScheduleDB extends DB {
    async addDepartment(name: string, connection: PoolConnection): Promise<number> {
        const query = <Query>{
            sql: 'INSERT INTO `departments` (`name`) VALUES (?);',
            values: [name]
        }
        const dbResponse = (await connection.queryAsync(query));
        return dbResponse.insertId;
    }

    async getScheduleList(department_id: number, connection: PoolConnection): Promise<any[]> {
        const query = <Query>{
            sql: 'SELECT \
                    schedules.is_current_schedule, \
                    UNIX_TIMESTAMP(schedules.start) as start, \
                    schedules.schedule_id, \
                    schedules.duration, \
                    schedules.department_id, \
                    departments.name \
                FROM schedules \
                JOIN departments \
                ON schedules.department_id=departments.department_id \
                WHERE departments.department_id=?;',
            values: [String(department_id)]
        }
        const dbResponse = (await connection.queryAsync(query)) as [];
        return dbResponse;
    }

    async addSchedule(start: string, duration: number, department_id: number, is_current_schedule: boolean): Promise<number> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'INSERT INTO `schedules` (`start`, `duration`, `department_id`, `is_current_schedule`) VALUES (?, ?, ?, ?);',
            values: [start, String(duration), String(department_id), is_current_schedule]
        }
        const dbResponse = (await connection.queryAsync(query));
        connection.release();
        return dbResponse.insertId;
    }

    async getSchedule(id: number): Promise<ScheduleDBResponse[]> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'SELECT schedules.schedule_id, UNIX_TIMESTAMP(schedules.start) as start, schedules.duration, \
                          people.person_id, people.name, UNIX_TIMESTAMP(days.day) as day,  \
                          days.day_schedule_type \
                  FROM schedules \
                  LEFT JOIN scheduled_people \
                  ON schedules.schedule_id=scheduled_people.schedule_id \
                  LEFT JOIN people \
                  ON people.person_id=scheduled_people.person_id \
                  LEFT JOIN days \
                  ON scheduled_people.id=days.scheduled_people_id \
                  WHERE schedules.schedule_id=?;',
            values: [String(id)]
        };
        const dbResponse = (await connection.queryAsync(query)) as ScheduleDBResponse[];
        connection.release();
        return dbResponse;
    }

    async getExistingPeople(schedule_id: number, connection: PoolConnection): Promise<[]> {
        const query = <Query>{
            sql: 'SELECT people.person_id, name, day, day_schedule_type FROM people \
                    JOIN scheduled_people \
                    ON people.person_id=scheduled_people.person_id \
                    JOIN days  \
                    ON scheduled_people.id=days.scheduled_people_id \
                    WHERE scheduled_people.schedule_id=?;',
            values: [String(schedule_id)]
        };
        const dbResponse = (await connection.queryAsync(query)) as [];
        return dbResponse;
    }

    async getWeekendBeforeSchedule(previousWeekend: string, connection: PoolConnection) {
        const previousSaturday = <Query>{
            sql: 'SELECT people.person_id, days.day FROM days \
                    INNER JOIN scheduled_people \
                    ON days.scheduled_people_id=scheduled_people.id \
                    INNER JOIN people \
                    ON scheduled_people.person_id=people.person_id \
                    WHERE day=?;',
            values: [previousWeekend]
        };
        const lastSaturdaySchedule: any = {};
        (await connection.queryAsync(previousSaturday))
            .map((x: any) => {
                if (!lastSaturdaySchedule[x.person_id]) {
                    lastSaturdaySchedule[x.person_id] = {};
                }
                lastSaturdaySchedule[x.person_id] = moment(x.day).unix();
            });
        return lastSaturdaySchedule;
    }

    async getScheduleInfo(scheduleId: number, connection: PoolConnection): Promise<Schedule> {
        const query = <Query>{
            sql: 'SELECT * FROM schedules \
                  WHERE schedule_id=?;',
            values: [String(scheduleId)]
        };
        const result = (await connection.queryAsync(query)) as Array<any>;
        if (!result || result.length === 0) {
            return null;
        } else {
            const startDay = moment(result[0].start).unix();
            const duration = result[0].duration;
            const schedule_id = result[0].schedule_id;
            return new Schedule(startDay, duration, schedule_id);
        }
    }

    async insertPerson(name: string, connection: PoolConnection) {
        const query = <Query>{
            sql: 'INSERT INTO people(name) VALUES (?);',
            values: [name]
        };
        const result = await connection.queryAsync(query);
        return result.insertId;
    }

    async schedulePerson(person_id: number, schedule_id: number, connection: PoolConnection) {
        const query = <Query>{
            sql: 'INSERT INTO scheduled_people(schedule_id, person_id) VALUES (?, ?);',
            values: [String(schedule_id), String(person_id)]
        };
        const result = await connection.queryAsync(query);
        return result.insertId;
    }

    async removeScheduledPerson(schedule_id: number, person_id: number) {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'DELETE FROM scheduled_people \
                  WHERE person_id=? AND schedule_id=?;',
            values: [String(person_id), String(schedule_id)]
        }
        const result = await connection.queryAsync(query);
        connection.release();
        return { deleted: result.affectedRows === 1, id: person_id };
    }

    async deletePerson(id: number): Promise<any> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'DELETE FROM people \
                  WHERE person_id=?;',
            values: [String(id)]
        }
        const result = await connection.queryAsync(query);
        connection.release();
        return { deleted: result.affectedRows === 1, id: id };
    }

    async deleteSchedule(id: number): Promise<any> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'DELETE FROM schedules \
                  WHERE schedule_id=?;',
            values: [String(id)]
        }
        const result = await connection.queryAsync(query);
        connection.release();
        return { deleted: result.affectedRows === 1, id: id };
    }

    async getScheduledPeopleId(schedule_id: number, person_id: number, connection: PoolConnection) {
        const dayExistsQuery = <Query>{
            sql: 'SELECT * FROM scheduled_people \
                    WHERE schedule_id=? and person_id=?;',
            values: [String(schedule_id), String(person_id)]
        };
        const id = (await connection.queryAsync(dayExistsQuery))[0].id;
        return id;
    }

    async insertDays(days: Array<[string, number]>, scheduled_people_id: number, connection: PoolConnection): Promise<any> {
        const values = [days.map(x => [x[0], moment.unix(x[1]).format('YYYY-MM-DD'), String(scheduled_people_id)])] as unknown;
        const query = <Query>{
            sql: 'INSERT INTO days(day_schedule_type, day, scheduled_people_id) VALUES ?;',
            values: values
        };
        const result = await connection.queryAsync(query);
        return result;
    }

    async getDayExists(date: string, person_id: number, connection: PoolConnection) {
        const dayExistsQuery = <Query>{
            sql: 'SELECT * FROM days \
                    INNER JOIN scheduled_people \
                    ON days.scheduled_people_id=scheduled_people.id  \
                    WHERE days.day=? and scheduled_people.person_id=?;',
            values: [date, person_id]
        };
        const exists = (await connection.queryAsync(dayExistsQuery)).length > 0;
        return exists;
    }

    async updateDayValue(newValue: DayScheduleType, id: number, date: string, connection: PoolConnection) {
        let response = { status: 'ERROR' }
        const dayUpdateQuery = <Query>{
            sql: 'UPDATE days \
                    INNER JOIN scheduled_people \
                    ON days.scheduled_people_id=scheduled_people.id  \
                    SET day_schedule_type = ? \
                    WHERE  scheduled_people.person_id=? AND days.day=?;',
            values: [newValue, id, date]
        };
        const result = await connection.queryAsync(dayUpdateQuery);
        if (result.affectedRows === 1 && result.changedRows === 1) {
            response.status = 'OK';
        }
        return response;
    }

    async getPeopleNotInSchedule(schedule_id: number) {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'SELECT people.person_id, name FROM people  \
                    WHERE people.person_id NOT IN ( \
                        SELECT people.person_id FROM people  \
                        INNER JOIN scheduled_people  \
                        ON people.person_id=scheduled_people.person_id \
                        WHERE scheduled_people.schedule_id=? \
                    ) GROUP BY people.person_id;',
            values: [String(schedule_id)]
        }
        const result = await connection.queryAsync(query);
        connection.release();
        return result;
    }

    async getPerson(person_id: number) {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'SELECT * FROM people \
                    WHERE person_id=?',
            values: [String(person_id)]
        };
        const result = await connection.queryAsync(query);
        connection.release();
        return result[0];
    }

    async addWorkPreference(work_preference: WorkPreference) {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'INSERT into work_preference(start, end, repeats, is_work, person_id) VALUES(?, ?, ?, ?, ?);',
            values: [work_preference.start, work_preference.end, work_preference.repeats,
            work_preference.is_work, work_preference.person_id]
        };
        const result = await connection.queryAsync(query);
        connection.release();
        return result.insertId;
    }

    async getWorkPreferences(person_id: number, connection: PoolConnection): Promise<WorkPreference[]> {
        const query = <Query>{
            sql: 'SELECT * FROM work_preference \
                    WHERE person_id=?;',
            values: [String(person_id)]
        };
        const result = (await connection.queryAsync(query)).map((work_preference: any) => {
            if (work_preference.start) {
                work_preference.start = DateTime.fromJSDate(work_preference.start).toFormat('yyyy-LL-dd');
            }
            if (work_preference.end) {
                work_preference.end = DateTime.fromJSDate(work_preference.end).toFormat('yyyy-LL-dd');
            }
            work_preference.is_work = work_preference.is_work === 1;
            return work_preference;
        });
        return result;
    }

    async deleteWorkPreference(preference_id: number): Promise<any> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'DELETE FROM work_preference \
                    WHERE id=?;',
            values: [String(preference_id)]
        };
        const result = await connection.queryAsync(query);
        connection.release();
        return { deletedRows: result.affectedRows };
    }
}