import {
    ConnectionConfig,
    createPool,
    Pool,
    PoolConnection
} from 'mysql';
import * as Bluebird from 'bluebird';

export class DB {

    protected pool: Pool;
    constructor(config: ConnectionConfig) {
        this.pool = Bluebird.promisifyAll(createPool(config));
    }

    endPool() {
        this.pool.end(error => {
            if (error) {
                console.log(error);
            }
        });
    }

    async getConnection(): Promise<PoolConnection> {
        return Bluebird.promisifyAll(await this.pool.getConnectionAsync());
    }
}