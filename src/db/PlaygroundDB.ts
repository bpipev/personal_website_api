import {
    PoolConnection,
    Query
} from 'mysql';
import { User } from '../models/User';
import { Plan } from '../models/Plan';
import { PlanStep } from '../models/PlanStep';
import { DB } from './DB';

export class PlaygroundDB extends DB {

    async getPublicPlans(planId: number): Promise<Plan> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql:
                'SELECT step_id, plan_steps.content AS plan_steps_content, plan_steps.plan_id, popularity, plans.content as plan_content, title, \
                 lat, lng, name \
                 FROM plan_steps \
                    INNER JOIN public_plans \
                ON public_plans.plan_id=plan_steps.plan_id \
                    INNER JOIN plans \
                ON public_plans.plan_id=plans.plan_id AND plans.plan_id=?;',
            values: [String(planId)]
        };
        let plan = {} as Plan;
        const dbRes = (await connection.queryAsync(query)) as any[];
        if (dbRes.length > 0) {
            plan = {
                plan_id: Number(dbRes[0].plan_id),
                title: String(dbRes[0].title),
                content: String(dbRes[0].plan_content),
                is_public: true
            };
            plan.steps = new Array<PlanStep>();
            for (let planStep of dbRes) {
                const location = planStep.lat && planStep.lng ? {
                    lat: planStep.lat, lng: planStep.lng,
                    name: String(planStep.name)
                } : null;
                plan.steps.push({
                    step_id: Number(planStep.step_id),
                    plan_id: Number(plan.plan_id),
                    content: String(planStep.plan_steps_content),
                    location: location
                });
            }
        }
        connection.release();
        return plan;
    }

    async getDashboardPlans(): Promise<Plan[]> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql:
                'SELECT * FROM plans INNER JOIN public_plans ON plans.plan_id=public_plans.plan_id;'
        };
        const dbRes = (await connection.queryAsync(query)) as Plan[];
        connection.release();
        return dbRes;
    }

    async getInsertUser(user: User): Promise<User> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'SELECT * FROM users as users WHERE users.login_id = ?;',
            values: [user.login_id]
        };
        const dbRes = (await connection.queryAsync(query)) as Array<any>;
        let result = <User>dbRes[0];

        if (!result) {
            result = await this._insertUser(connection, user);
        }
        connection.release();
        return result;
    }

    async getUserById(user: User): Promise<User> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'SELECT * FROM users as users WHERE users.user_id = ?;',
            values: [String(user.user_id)]
        };
        const dbRes = (await connection.queryAsync(query)) as Array<any>;
        const result = <User>dbRes[0];
        connection.release();
        return result;
    }

    private async _insertUser(
        connection: PoolConnection,
        user: User
    ): Promise<User> {
        const query = <Query>{
            sql: 'INSERT INTO users(login_id, login_type) VALUES(?, ?);',
            values: [user.login_id, user.login_type]
        };
        const result = await connection.queryAsync(query);
        return user;
    }

    async insertUser(user: User): Promise<User> {
        const connection = await this.getConnection();
        const result = await this._insertUser(connection, user);
        connection.release();
        return user;
    }

    /**
     * Returns all of a user's plans
     * @param userId Id of the user
     */
    async getAllUserPlans(userId: number): Promise<Plan[]> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql:
                'SELECT plans.*, NOT(ISNULL(public_plans.plan_id)) as is_public_bit \
                 FROM (SELECT * FROM plans WHERE user_id=?) as plans \
                 LEFT JOIN public_plans ON plans.plan_id=public_plans.plan_id;',
            values: [String(userId)]
        };
        const result = (await connection.queryAsync(query)) as Plan[];
        result.forEach(plan => {
            plan.is_public = plan.is_public_bit === 1 ? true : false;
            return plan;
        });
        connection.release();
        return result;
    }

    async insertPlan(plan: Plan): Promise<Plan> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'INSERT INTO plans(content, title, user_id) VALUES(?, ?, ?);',
            values: [plan.content, plan.title, plan.user_id]
        };
        const result = await connection.queryAsync(query);
        plan.plan_id = result.insertId;
        plan.steps.forEach(element => {
            element.plan_id = result.insertId;
        });
        if (plan.steps != null && plan.steps.length > 0) {
            await this._insertSteps(
                connection,
                plan.user_id,
                plan.plan_id,
                plan.steps
            );
        }
        connection.release();
        return plan;
    }

    async deletePlan(userId: number, planId: number): Promise<any> {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'DELETE FROM plans WHERE plan_id=? AND user_id=?;',
            values: [String(planId), String(userId)]
        };
        const result = await connection.queryAsync(query);
        connection.release();
        return result;
    }

    async getPlanWithSteps(userId: number, planId: number): Promise<Plan> {
        const connection = await this.getConnection();
        const plan = await this._getPlanWithSteps(userId, planId, connection);
        connection.release();
        return plan;
    }

    async makePlanPublic(userId: number, planId: number): Promise<boolean> {
        const connection = await this.getConnection();
        let result = false;
        if (await this.ownsPlan(connection, userId, planId)) {
            const query = <Query>{
                sql:
                    'INSERT INTO public_plans(popularity, plan_id) VALUES(?, ?);',
                values: [0, String(planId)]
            };
            result = (await connection.queryAsync(query)).affectedRows === 1;
        }
        connection.release();
        return result;
    }

    async makePlanPrivate(userId: number, planId: number): Promise<boolean> {
        const connection = await this.getConnection();
        let result = false;
        if (this.ownsPlan(connection, userId, planId)) {
            const query = <Query>{
                sql: 'DELETE FROM public_plans WHERE plan_id=?;',
                values: [String(planId)]
            };
            result = (await connection.queryAsync(query)).affectedRows === 1;
        }
        connection.release();
        return result;
    }

    private async _getPlanWithSteps(
        userId: number,
        planId: number,
        connection: PoolConnection
    ) {
        const query = <Query>{
            sql:
                'SELECT plan_steps.step_id, plan_steps.content, plan_steps.plan_id, plan_steps.lat, plan_steps.lng, plan_steps.name \
                 FROM plan_steps \
                 INNER JOIN plans ON plan_steps.plan_id=plans.plan_id AND plans.user_id=? AND plan_steps.plan_id=?;',
            values: [String(userId), String(planId)]
        };
        const plan = ((await this.getPlan(connection, planId)) as Plan[])[0];
        const result = (await connection.queryAsync(query)).map((x: any) => {
            const location = x.lat && x.lng ? { lat: x.lat, lng: x.lng, name: x.name } : null;
            return <PlanStep>{ content: x.content, plan_id: x.plan_id, step_id: x.step_id, location: location }
        });
        plan.steps = result;
        return plan;
    }

    private async getPlan(
        connection: PoolConnection,
        planId: number
    ): Promise<Plan[]> {
        const query = <Query>{
            sql: 'SELECT * FROM plans WHERE plan_id=?;',
            values: [String(planId)]
        };
        const result = (await connection.queryAsync(query)) as Plan[];
        return result;
    }

    async insertSteps(
        userId: number,
        planId: number,
        steps: PlanStep[]
    ): Promise<PlanStep[]> {
        const connection = await this.getConnection();
        await this._insertSteps(connection, userId, planId, steps);
        connection.release();
        return steps;
    }

    private async _insertSteps(
        connection: PoolConnection,
        userId: number,
        planId: number,
        steps: PlanStep[]
    ): Promise<PlanStep[]> {
        const rows = [
            steps.map(step => {
                return [step.content, String(step.plan_id), step.location?.lat, step.location?.lng, step.location?.name];
            })
        ] as unknown;
        if (await this.ownsPlan(connection, userId, planId)) {
            const query = <Query>{
                sql: 'INSERT INTO plan_steps(content, plan_id, lat, lng, name) VALUES ?;',
                values: rows
            };
            const result = await connection.queryAsync(query);
            steps.forEach(step => {
                // What happens if multiple requests are handled at the same time? Will the ID mess up? Maybe it's better to add one by one
                step.step_id = result.insertId++;
            });
        } else {
            throw new Error('User does not own all plans.');
        }
        return steps;
    }

    async deleteStep(userId: number, stepId: number): Promise<any> {
        const connection = await this.getConnection();
        let result = null;
        if (await this.ownsPlanWithStep(connection, userId, stepId)) {
            const query = <Query>{
                sql: 'DELETE FROM plan_steps WHERE step_id=?;',
                values: [String(stepId)]
            };
            let result = await connection.queryAsync(query);
        } else {
            throw new Error('User does not own plan.');
        }
        connection.release();
        return result;
    }

    async ownsPlanWithStep(
        connection: PoolConnection,
        userId: number,
        stepId: number
    ): Promise<boolean> {
        // TODO: Check if there's a better way to verify user owns plan steps.
        const query = <Query>{
            sql:
                'SELECT * FROM plan_steps \
                 INNER JOIN plans \
                 ON plans.plan_id=plan_steps.plan_id AND plans.user_id=? AND plan_steps.step_id=?;',
            values: [String(userId), stepId]
        };
        const result = (await connection.queryAsync(query)) as Array<any>;
        return result.length > 0;
    }

    async ownsPlan(
        connection: PoolConnection,
        userId: number,
        planId: number
    ): Promise<boolean> {
        // TODO: Check if there's a better way to verify user owns plan.
        const query = <Query>{
            sql:
                'SELECT * FROM plans \
                 WHERE plans.user_id=? AND plans.plan_id=?;',
            values: [String(userId), planId]
        };
        const result = (await connection.queryAsync(query)) as Array<any>;
        return result.length > 0;
    }
}
