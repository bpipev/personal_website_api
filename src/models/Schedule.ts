import { DateTime, Interval } from 'luxon';
import { WorkPreference } from './WorkPreference';

export interface Person {
    name: string;
    schedule_id: number;
    schedule_type: ScheduleLength;
    work_week_hours: number;
    id?: number;
}

export const UNIX_DAY = 86400;

export class Schedule {
    public id: number;
    public dates: Array<number>;
    public data: any = {};
    public currentUser: number;
    private _startDate: number;

    public get startDate(): number {
        return this._startDate;
    }

    constructor(startDate: number, length: number, schedule_id: number) {
        this.data = {};
        this.dates = new Array<number>();
        this._startDate = startDate;
        for (let i = 0; i < length; i++) {
            let date = DateTime.fromSeconds(startDate);
            date = date.plus({ days: i });
            this.dates.push(date.toSeconds());
        }
        this.id = schedule_id;
    }

    /**
     * @param scheduleForDay Array of day types (e.g. ["D", "E", "E", ...])
     * @param scheduleType Type of schedule
     */
    private canAddDay(scheduleForDay: Array<string>, scheduleType: ScheduleLength): DayScheduleType {
        let d = 0;
        let e = 0;
        let n = 0;
        let e1 = 0;
        let e2 = 0;
        scheduleForDay.forEach(element => {
            switch (element) {
                case DayScheduleType.D:
                    d += 1;
                    break;
                case DayScheduleType.E:
                    e += 1;
                    break;
                case DayScheduleType.N:
                    n += 1;
                    break;
                case DayScheduleType.J:
                    d += 1;
                    e1 += 1;
                    break;
                case DayScheduleType.Z:
                    n += 1;
                    e2 += 1;
                    break;
                default:
                    break;
            }
        });
        while (e1 > 0 && e2 > 0) {
            e += 1;
            e1--;
            e2--;
        }
        let result: DayScheduleType;
        if (scheduleType === ScheduleLength.HOURS_8) {
            if (d < 4) {
                result = DayScheduleType.D;
            } else if (e < 4) {
                result = DayScheduleType.E;
            } else if (n < 3) {
                result = DayScheduleType.N;
            }
        } else {
            if (d < 4 && e < 4) {
                result = DayScheduleType.J;
            } else if (e < 4 && n < 3) {
                result = DayScheduleType.Z;
            }
        }
        return result;
    }

    addPerson(person: Person, weekendBeforeSchedule: any, workPreferences: Array<WorkPreference>): Array<[string, number]> {
        let person_schedule = new Array<[string, number]>();
        this.data[person.id] = { workdays: {} };
        if (person.schedule_type === ScheduleLength.HOURS_8) {
            let start = 0;
            let end = 7;
            while (end <= this.dates.length) {
                const daysOfWeek = this.dates.slice(start, end);
                person_schedule = person_schedule.concat(this.schedule8HourShift(person, daysOfWeek, weekendBeforeSchedule, workPreferences));
                start += 7;
                end += 7;
            }
        } else {
            let start = 0;
            let end = 14;
            while (end <= this.dates.length) {
                const daysOfWeek = this.dates.slice(start, end);
                // TODO: work preference
                person_schedule = person_schedule.concat(this.schedule12HourShift(person, daysOfWeek, weekendBeforeSchedule));
                start += 14;
                end += 14;
            }
        }
        return person_schedule;
    }

    addExistingScheduledDay(name: string, day: number, day_schedule_type: DayScheduleType, id: number) {
        if (!(id in this.data)) {
            this.data[id] = { workdays: {}, name: name };
        }
        this.data[id].workdays[day] = day_schedule_type;
    }

    private schedule8HourShift(person: Person, week: Array<number>, lastWeekendSchedule: any, workPreferences: Array<WorkPreference>): Array<[string, number]> {
        // Order by priority
        const priority_week = this.order(week, this.data, workPreferences);
        const week_schedule = new Array<[string, number]>();
        let remainingHours = person.work_week_hours;
        priority_week.forEach(day => {
            const currentDate = DateTime.fromSeconds(day);
            const dayOfWeek = currentDate.weekday;
            for (let preference of workPreferences) {
                const start = DateTime.fromFormat(preference.start, 'yyyy-MM-dd');
                const end = DateTime.fromFormat(preference.end, 'yyyy-MM-dd');
                if (this.overlapsWithPreference(start, end, currentDate)) {
                    if (!preference.is_work) {
                        // On Vacation or off work
                        return;
                    }
                }
            }
            if (dayOfWeek === 6 || dayOfWeek === 7) {
                // It's the weekend
                if (person.id in lastWeekendSchedule) {
                    // Person worked last weekend before schedule started
                    const lastWeekend = DateTime.fromSeconds(lastWeekendSchedule[person.id]);
                    const interval = Interval.fromDateTimes(lastWeekend, currentDate).length('days');
                    if (interval > 7) {
                        // still weekend, need to check if person worked last weekend in schedule
                        remainingHours = this.scheduleWeekend(currentDate, person, remainingHours, day, week_schedule);
                    } else if (interval == 1) {
                        // person worked saturday, today is sunday and should work again
                        remainingHours = this.scheduleWeekend(currentDate, person, remainingHours, day, week_schedule);
                    }
                } else {
                    // person did not work last weekend
                    remainingHours = this.scheduleWeekend(currentDate, person, remainingHours, day, week_schedule);
                }
            } else {
                remainingHours = this.scheduleDay(day, person, remainingHours, week_schedule);
            }
        });
        return week_schedule;
    }

    private hasNotWorkedPreviousWeekend(date: DateTime, person_id: number) {
        const previousSunday = date.minus({ days: date.weekday });
        const previousSaturday = previousSunday.minus({ days: 1 });
        return !this.data[person_id].workdays[previousSunday.toSeconds()] && !this.data[person_id].workdays[previousSaturday.toSeconds()]
    }

    private willNotWorkNextWeekend(date: DateTime, person_id: number) {
        const nextSundayDays = 14 - date.weekday;
        const nextSunday = date.plus({ days: nextSundayDays });
        const nextSaturday = nextSunday.minus({ days: 1 });
        return !this.data[person_id].workdays[nextSunday.toSeconds()] && !this.data[person_id].workdays[nextSaturday.toSeconds()];
    }

    private scheduleWeekend(date: DateTime, person: Person, remainingHours: number, day: number, week_schedule: [string, number][]) {
        if (this.hasNotWorkedPreviousWeekend(date, person.id) && this.willNotWorkNextWeekend(date, person.id)) {
            remainingHours = this.scheduleDay(day, person, remainingHours, week_schedule);
        }
        return remainingHours;
    }

    private schedule12Weekend(date: DateTime, person: Person, scheduleForDay: any[], canAdd8hrDay: boolean, dayType: DayScheduleType) {
        if (this.hasNotWorkedPreviousWeekend(date, person.id) && this.willNotWorkNextWeekend(date, person.id)) {
            const hours8day = this.canAddDay(scheduleForDay, ScheduleLength.HOURS_8);
            if (hours8day && canAdd8hrDay) {
                dayType = hours8day;
                canAdd8hrDay = false;
            } else {
                dayType = this.canAddDay(scheduleForDay, person.schedule_type);
            }
        }
        return { canAdd8hrDay, dayType };
    }

    private scheduleDay(day: number, person: Person, remainingHours: number, week_schedule: [string, number][]) {
        // get array of day types for current day
        const scheduleForDay = this.getScheduleForDay(day);
        const dayType = this.canAddDay(scheduleForDay, person.schedule_type);
        if (remainingHours > 0) {
            if (dayType) {
                this.data[person.id].workdays[day] = dayType;
                week_schedule.push([dayType, day]);
                remainingHours -= this.getHours(dayType);
            }
        }
        return remainingHours;
    }

    private schedule12HourShift(person: Person, days: Array<number>, weekendBeforeSchedule: any): Array<[string, number]> {
        let canAdd8hrDay = true;
        // Order by priority
        const prioritySchedule = this.order(days, this.data);
        const schedule = new Array<[string, number]>();
        let remainingHours = person.work_week_hours * 2;
        prioritySchedule.forEach(day_unix_timestamp => {
            const scheduleForDay = this.getScheduleForDay(day_unix_timestamp);
            let dayType: DayScheduleType;
            const previousDay = DateTime.fromSeconds(day_unix_timestamp).minus({ days: 1 });
            const previous2Day = previousDay.minus({ days: 1 });
            const nextDay = DateTime.fromSeconds(day_unix_timestamp).plus({ days: 1 });
            const next2Day = nextDay.plus({ days: 1 });
            // Can't schedule three consecutive 12 hour shifts - J or Z
            const works_previous_day_1 = this.worksOnDay(schedule, previousDay);
            const works_previous_day_2 = this.worksOnDay(schedule, previous2Day);
            const works_next_day_1 = this.worksOnDay(schedule, nextDay);
            const works_next_day_2 = this.worksOnDay(schedule, next2Day);
            const hasNotWorkedPreviousConsecutiveDays = !works_previous_day_1 || !works_previous_day_2;
            const hasNotWorkedNextConsecutiveDays = !works_next_day_1 || !works_next_day_2;
            if (hasNotWorkedPreviousConsecutiveDays && hasNotWorkedNextConsecutiveDays) { // No J or Z in previous/next two days
                const date = DateTime.fromSeconds(day_unix_timestamp);
                const dayOfWeek = date.weekday;
                // If it's the weekend
                if (dayOfWeek === 6 || dayOfWeek === 7) {
                    if (person.id in weekendBeforeSchedule) { // Worked last weekend before current schedule
                        const start = DateTime.fromSeconds(weekendBeforeSchedule[person.id]);
                        const interval = Interval.fromDateTimes(start, date);
                        if (interval.length('days') > 7) { // 
                            ({ canAdd8hrDay, dayType } = this.schedule12Weekend(date, person, scheduleForDay, canAdd8hrDay, dayType));
                        }
                    } else {
                        ({ canAdd8hrDay, dayType } = this.schedule12Weekend(date, person, scheduleForDay, canAdd8hrDay, dayType));
                    }
                } else {
                    // need variation between day/evening?
                    const hours8day = this.canAddDay(scheduleForDay, ScheduleLength.HOURS_8);
                    if (hours8day && canAdd8hrDay) {
                        dayType = hours8day;
                        canAdd8hrDay = false;
                    } else {
                        dayType = this.canAddDay(scheduleForDay, person.schedule_type);
                    }
                }
            }
            if (remainingHours > 0) {
                if (dayType) {
                    this.data[person.id].workdays[day_unix_timestamp] = dayType;
                    schedule.push([dayType, day_unix_timestamp]);
                    remainingHours -= this.getHours(dayType);
                }
            }
        });
        return schedule;
    }

    

    private worksOnDay(schedule: [string, number][], previousDay: DateTime) {
        return schedule.find(x => x[1] === previousDay.toSeconds() &&
            ['D', 'E', 'N'].indexOf(x[0]) === -1);
    }

    private getHours(day_type: DayScheduleType) {
        if (day_type) {
            if (['D', 'E', 'N'].indexOf(day_type) > -1) {
                return 8;
            } else {
                return 12;
            }
        } else {
            return 0;
        }
    }

    private order(days: number[], data: any, workPreferences: WorkPreference[] = []) {
        const priority_total = Object.create(null);
        // Calculate priorities for whole schedule
        Object.keys(data).forEach(element => {
            Object.keys(data[element].workdays).forEach(dataItem => {
                if (!priority_total[dataItem]) {
                    priority_total[dataItem] = 0;
                }
                priority_total[dataItem] += 1;
            });
        });
        // Calcualate and order by priority for current days
        // for existing/occupied schedule by other people
        let priority_week = Object.keys(priority_total)
            .map(x => [x, priority_total[x]])
            .sort((a, b) => {
                if (a[1] === b[1]) {
                    if (a[0] === b[0]) {
                        return 0;
                    }
                    const day1 = DateTime.fromSeconds(Number(a[0]));
                    const day2 = DateTime.fromSeconds(Number(b[0]));
                    const weekday1 = day1.weekday;
                    const weekday2 = day2.weekday;
                    const result = this.overlapCompare(day1, day2, workPreferences) || (weekday1 > weekday2 ? -1 : 1);
                    return result;
                }
                return a[1] - b[1];
            })
            .map(x => Number(x[0]))
            .filter(x => days.indexOf(x) >= 0);
        days.sort((a, b) => {
            if (a === b) {
                return 0;
            }
            const day1 = DateTime.fromSeconds(Number(a));
            const day2 = DateTime.fromSeconds(Number(b));
            const weekday1 = day1.weekday;
            const weekday2 = day2.weekday;
            //                                 Add Sunday/Saturday first to schedule weekend first
            const result = this.overlapCompare(day1, day2, workPreferences) || (weekday1 > weekday2 ? -1 : 1);
            return result;
        })
        // priority_week = [...new Set(days.concat(priority_week))];
        // TODO: improve order... for now it groups by (1. No other people working 2. preference)
        priority_week = days.filter(item => priority_week.indexOf(item) < 0).concat(priority_week);
        return priority_week;
    }

    private overlapCompare(day1: DateTime, day2: DateTime, workPreferences: WorkPreference[]) {
        let firstOverlaps = false;
        let secondOverlaps = false;
        let isWork = false;
        for (let preference of workPreferences) {
            const start = DateTime.fromFormat(preference.start, 'yyyy-MM-dd');
            const end = DateTime.fromFormat(preference.end, 'yyyy-MM-dd');
            // TODO: handle repeats/overlaps
            firstOverlaps = this.overlapsWithPreference(start, end, day1);
            secondOverlaps = this.overlapsWithPreference(start, end, day2);
            isWork = preference.is_work;
            if (firstOverlaps || secondOverlaps)
                break;
        }
        if (firstOverlaps) {
            return isWork ? -1 : 1;
        } else if (secondOverlaps) {
            return isWork ? 1 : -1;
        } else {
            return 0;
        }
    }

    private overlapsWithPreference(preferenceStart: DateTime, preferenceEnd: DateTime, day: DateTime) {
        const interval = Interval.fromDateTimes(preferenceStart, preferenceEnd);
        return interval.contains(day) || preferenceStart.startOf('day').equals(day.startOf('day')) || preferenceEnd.startOf('day').equals(day.startOf('day'));
    }

    private getScheduleForDay(day_unix_timestamp: number) {
        return Object.keys(this.data).map(x => this.data[x].workdays[day_unix_timestamp]);
    }
}

export enum DayScheduleType {
    D = 'D',
    E = 'E',
    N = 'N',
    J = 'J',
    Z = 'Z',
    E1 = 'E1',
    E2 = 'E2'
}

export interface ScheduleDBResponse {
    schedule_id: number,
    start: number,
    duration: number,
    person_id: number,
    name: string,
    day_schedule_type: string,
    day: number
}

export enum ScheduleLength {
    HOURS_8 = 8,
    HOURS_12 = 12
}
