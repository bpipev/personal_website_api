export interface User {
    login_id: string;
    login_type: string;
    user_id: number;
}
