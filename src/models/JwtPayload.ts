export interface jwtPayload {
    aud: string;
    exp: number;
    iat: number;
    iss: string;
    sub: string;
}
