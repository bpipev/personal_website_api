import { Plan } from './Plan';

export interface Dashboard {
    plans: Array<Plan>;
}
