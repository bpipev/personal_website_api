import { PlaygroundDB } from '../db/PlaygroundDB';
import { ConnectionConfig } from 'mysql';
import { ScheduleDB } from '../db/ScheduleDB';

export class Database {
    private static playgroundDb: PlaygroundDB;
    private static scheduleDb: ScheduleDB;
    private constructor() {}
    static getPlayground(config: ConnectionConfig): PlaygroundDB {
        if (this.playgroundDb == null) {
            this.playgroundDb = new PlaygroundDB(config);
        }
        return this.playgroundDb;
    }
    static getScheduleDB(config: ConnectionConfig): ScheduleDB {
        if (this.scheduleDb == null) {
            this.scheduleDb = new ScheduleDB(config);
        }
        return this.scheduleDb;
    }
}
