export interface PlanStep {
    step_id: number;
    content: string;
    plan_id: number;
    location?: { lat: number, lng: number, name: string}
}
