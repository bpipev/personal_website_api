interface FacebookResponse {
    data: {
        app_id: string;
        application: string;
        data_access_expires_at: number;
        expires_at: number;
        is_valid: boolean;
        scopes: Array<string>;
        type: string;
        user_id: string;
    };
}

export { FacebookResponse };
