import { PlanStep } from './PlanStep';

export interface Plan {
    user_id?: number;
    title: string;
    content: string;
    plan_id: number;
    steps?: PlanStep[];
    is_public: boolean;
    is_public_bit?: number;
}
