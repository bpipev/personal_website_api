FROM node:10.15.3
ENV TS_NODE_FILES=true
WORKDIR /app
# Copy package.json first and install npm deps to cache it
COPY package.json .
COPY package-lock.json .
RUN npm install
COPY . .