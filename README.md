This project attempts to use architecture similar to https://www.coreycleary.me/project-structure-for-an-express-rest-api-when-there-is-no-standard-way

# Test setup:
* Add ```TS_NODE_FILES=true``` to env variables for chai + mocha to compile project typings
* Add testuser to DB
* create config-test.json 
```json
{
    "PLAYGROUND_HOST": "localhost",
    "PLAYGROUND_USER": "testuser",
    "PLAYGROUND_PASS": "<password>",
    "PLAYGROUND_DB": "playground_test"
}
```
* setup ReportPortal: curl https://raw.githubusercontent.com/reportportal/reportportal/master/docker-compose.yml -o docker-compose.yml and follow instructions inside
* OR docker-compose -f docker-compose.yml -p reportportal up -d --force-recreate
* Create/update config-report-portal.json
```json
{
    "token": "<access_token from User Profile>",
    "endpoint": "http://localhost:8080/api/v1",
    "launch": "LAUNCH_NAME",
    "project": "superadmin_personal"
}
```
* start jenkins: 
```powershell
docker run `
  -u root `
  --rm `
  -d `
  -p 8081:8080 `
  -p 50000:50000 `
  -v jenkins-data:/var/jenkins_home `
  -v /var/run/docker.sock:/var/run/docker.sock `
  -t jenkinsci/blueocean
```
* set jenkins URL: http://host.docker.internal:8081/
* Create a jenkins multibranch pipeline + add bitbucket app password
* add secret files to project config-test.json and config-report-portal.json using host.docker.internal instead of localhost
* add agent to docker
* build slave with docker:
```powershell
cd jenkins-slave
```
```powershell
docker build . -t jenkins/slave-docker
```
* Configure master to "Only build jobs with label expressions matching this node"
* start agent:
```powershell
docker run -i --rm --name agent1 --init -v /var/run/docker.sock:/var/run/docker.sock jenkins/slave-docker java -jar /usr/share/jenkins/slave.jar -jnlpUrl http://host.docker.internal:8081/computer/Agent/slave-agent.jnlp -secret <secret>
```