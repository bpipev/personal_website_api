import { Test } from 'mocha';
import { readFileSync } from 'fs';
import { PlaygroundDB } from '../src/db/PlaygroundDB';
import { Query } from 'mysql';
import { ScheduleDB } from '../src/db/ScheduleDB';

const RPClient: any = require('@reportportal/client-javascript');

class RPClientHelper {
    private static launchObj: any = null;
    private static suiteObj: any = null;
    private static stepObj: any = null;
    private static rpClient = new RPClient(
        JSON.parse(readFileSync('./config-report-portal.json', 'utf-8'))
    );

    private constructor() {}

    static async before() {
        try {
            const response = await this.rpClient.checkConnect();
            console.log('You have successfully connected to the server.');
            console.log(`You are using an account: ${JSON.stringify(response)}`);
            this.launchObj = this.rpClient.startLaunch({
                name: 'Client test',
                start_time: this.rpClient.helpers.now(),
                description: 'description of the launch',
                tags: ['tag1', 'tag2']
                //this param used only when you need client to send data into the existing launch
                // id: 'id'
            });
        } catch (error) {
            console.log('Error connection to server');
            console.dir(error);
        }
    }
    static after() {
        this.rpClient.finishLaunch(this.launchObj.tempId, {
            end_time: this.rpClient.helpers.now()
        });
    }

    static beforeSuite(description: string, name: string) {
        this.suiteObj = this.rpClient.startTestItem(
            {
                description: description,
                name: name,
                start_time: this.rpClient.helpers.now(),
                type: 'SUITE'
            },
            this.launchObj.tempId
        );
    }
    static afterSuite() {
        this.rpClient.finishTestItem(this.suiteObj.tempId, {});
    }

    static beforeTest(currentTest: Test) {
        this.stepObj = this.rpClient.startTestItem(
            {
                description: currentTest.fullTitle(),
                name: currentTest.title,
                start_time: this.rpClient.helpers.now(),
                tags: ['step_tag', 'step_tag2', 'step_tag3'],
                type: 'STEP'
            },
            this.launchObj.tempId,
            this.suiteObj.tempId
        );
    }

    static afterTest(currentTest: Test, stack: string) {
        this.rpClient.sendLog(this.stepObj.tempId, {
            level: 'TRACE',
            message: stack,
            time: this.rpClient.helpers.now()
        });
        this.rpClient.finishTestItem(this.stepObj.tempId, {
            status: currentTest.state
        });
    }
}

// Global before
before(async () => {
    await RPClientHelper.before();
});

// Global after
after(() => {
    RPClientHelper.after();
});

class PlaygroundDBhelper extends PlaygroundDB {
    async recreate() {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'DROP DATABASE IF EXISTS playground_test'
        };
        await connection.queryAsync(query);

        const queryCreate = <Query>{
            sql: 'CREATE DATABASE IF NOT EXISTS playground_test;'
        };
        await connection.queryAsync(queryCreate);
        const useDb = <Query>{
            sql: 'USE playground_test;'
        };
        await connection.queryAsync(useDb);

        const users = <Query>{
            sql:
                'CREATE TABLE IF NOT EXISTS users (             \
                user_id INT AUTO_INCREMENT PRIMARY KEY,     \
                login_id varchar(64),                       \
                login_type VARCHAR(10)                      \
            );'
        };
        await connection.queryAsync(users);

        const plans = <Query>{
            sql:
                'CREATE TABLE IF NOT EXISTS plans (             \
                plan_id INT AUTO_INCREMENT PRIMARY KEY,     \
                content VARCHAR(255),                       \
                title VARCHAR(255),                         \
                user_id INT,                                \
                FOREIGN KEY (user_id) REFERENCES users(user_id) \
            );'
        };
        await connection.queryAsync(plans);

        const planSteps = <Query>{
            sql:
                'CREATE TABLE IF NOT EXISTS plan_steps (        \
                step_id INT AUTO_INCREMENT PRIMARY KEY,     \
                content VARCHAR(255),                       \
                plan_id INT,                                \
                name VARCHAR(255),                        \
                lat DECIMAL(10, 8),                        \
                lng DECIMAL(11, 8),                           \
                FOREIGN KEY (plan_id) REFERENCES plans(plan_id) ON DELETE CASCADE \
            );'
        };
        await connection.queryAsync(planSteps);

        const pubblicPlans = <Query>{
            sql:
                'CREATE TABLE IF NOT EXISTS public_plans (      \
                popularity INT,                             \
                plan_id INT,                                \
                FOREIGN KEY (plan_id) REFERENCES plans(plan_id) ON DELETE CASCADE \
            );'
        };
        await connection.queryAsync(pubblicPlans);
        connection.release();
    }
}

class ScheduleDBhelper extends ScheduleDB {
    async recreate() {
        const connection = await this.getConnection();
        const query = <Query>{
            sql: 'DROP DATABASE IF EXISTS scheduledb_test'
        };
        await connection.queryAsync(query);

        const queryCreate = <Query>{
            sql: 'CREATE DATABASE IF NOT EXISTS scheduledb_test;'
        };
        await connection.queryAsync(queryCreate);
        const useDb = <Query>{
            sql: 'USE scheduledb_test;'
        };
        await connection.queryAsync(useDb);

        const departments = <Query>{
            sql:
                'CREATE TABLE IF NOT EXISTS departments ( \
                    department_id INT AUTO_INCREMENT PRIMARY KEY, \
                    name VARCHAR(255) \
                );'
        };
        await connection.queryAsync(departments);

        const schedules = <Query>{
            sql:
                'CREATE TABLE IF NOT EXISTS schedules ( \
                    schedule_id INT AUTO_INCREMENT PRIMARY KEY, \
                    start DATE, \
                    duration SMALLINT, \
                    department_id INT, \
                    is_current_schedule BOOL, \
                    FOREIGN KEY (department_id) REFERENCES departments(department_id) ON DELETE CASCADE \
                );'
        };
        await connection.queryAsync(schedules);

        const people = <Query>{
            sql:
                'CREATE TABLE IF NOT EXISTS people ( \
                    person_id INT AUTO_INCREMENT PRIMARY KEY,  \
                    name VARCHAR(255) \
                );'
        };
        await connection.queryAsync(people);

        const scheduled_people = <Query>{
            sql:
                'CREATE TABLE IF NOT EXISTS scheduled_people ( \
                    id INT AUTO_INCREMENT PRIMARY KEY, \
                    schedule_id INT, \
                    person_id INT, \
                    FOREIGN KEY (schedule_id) REFERENCES schedules(schedule_id) ON DELETE CASCADE, \
                    FOREIGN KEY (person_id) REFERENCES people(person_id) ON DELETE CASCADE \
                );'
        };
        await connection.queryAsync(scheduled_people);

        const work_preference = <Query>{
            sql:
                'CREATE TABLE IF NOT EXISTS work_preference ( \
                    id INT AUTO_INCREMENT PRIMARY KEY, \
                    start DATE, \
                    end DATE, \
                    repeats ENUM(\'weekly\', \'biweekly\', \'monthly\'), \
                    is_work BOOL, \
                    person_id INT, \
                    FOREIGN KEY (person_id) REFERENCES people(person_id) ON DELETE CASCADE \
                );'
        };
        await connection.queryAsync(work_preference);

        const days = <Query>{
            sql:
                'CREATE TABLE IF NOT EXISTS days ( \
                    day_id INT AUTO_INCREMENT PRIMARY KEY, \
                    scheduled_people_id INT, \
                    day DATE, \
                    day_schedule_type VARCHAR(7), \
                    FOREIGN KEY (scheduled_people_id) REFERENCES scheduled_people(id) ON DELETE CASCADE \
                );'
        };
        await connection.queryAsync(days);
        connection.release();
    }
}

export { RPClientHelper, PlaygroundDBhelper, ScheduleDBhelper };
