import { expect } from 'chai';
import { Configuration } from '../src/config/config';
import { RPClientHelper, PlaygroundDBhelper } from './helper';
import { User } from '../src/models/User';

// TS_NODE_FILES env variable must be true in order to compile project typings

describe('PlaygroundDB', function () {
    let db: PlaygroundDBhelper = null;
    before(() => {
        RPClientHelper.beforeSuite('Playground Database tests', 'PlaygroundDatabase');
        const config = new Configuration('./config-test.json');
        db = new PlaygroundDBhelper(config.getDatabaseTestConfig());
    });
    beforeEach(async function () {
        try {
            await db.recreate();
        } catch (e) {
            console.log(e.message);
        }
        RPClientHelper.beforeTest(this.currentTest);
    });
    afterEach(function () {
        RPClientHelper.afterTest(
            this.currentTest,
            this.currentTest.err ? this.currentTest.err.stack : ''
        );
    });

    after(() => {
        RPClientHelper.afterSuite();
        db.endPool();
    });

    describe('#getPlayground', () => {
        it('should return a non-null object', () => {
            expect(db).to.not.be.null;
        });
    });

    describe('PlaygroundDB#getPublicPlans', () => {
        it('should return an empty object', async () => {
            const planId = 0;
            const resultPlans = await db.getPublicPlans(planId);
            expect(resultPlans).to.be.empty;
        });
    });

    describe('PlaygroundDB#getDashboardPlans', () => {
        it('should return an empty object', async () => {
            const resultPlans = await db.getDashboardPlans();
            expect(resultPlans).to.be.empty;
        });
    });

    describe('PlaygroundDB#getInsertUser', () => {
        it('should return a user', async () => {
            const user = <User>{
                login_id: '10000',
                login_type: 'GOOGLE'
            };
            const resultUser = await db.getInsertUser(user);
            expect(resultUser).to.not.be.empty;
            expect(resultUser.login_id).to.equal(user.login_id);
            expect(resultUser.login_type).to.equal(user.login_type);
            expect(resultUser.user_id).to.not.be.equal(1);
        });
    });

    describe('PlaygroundDB#getUserById', () => {
        it('should return a user', async () => {
            const user = <User>{
                login_id: '10000',
                login_type: 'GOOGLE'
            };
            const result1 = await db.getInsertUser(user);
            const result2 = await db.getUserById(<User>{ user_id: 1 });
            user.user_id = 1;
            expect(result1).to.deep.equal(result2).to.deep.equal(user);
        });
    });

    describe('PlaygroundDB#getAllUserPlans', () => {
        it('should return an empty object', async () => {
            const resultPlans = await db.getAllUserPlans(1);
            expect(resultPlans).to.be.empty;
        });
    });
});
