import { expect } from 'chai';
import { Configuration } from '../src/config/config';
import { RPClientHelper, ScheduleDBhelper } from './helper';

// TS_NODE_FILES env variable must be true in order to compile project typings

describe('ScheduleDB', function () {
    let db: ScheduleDBhelper = null;
    before(() => {
        RPClientHelper.beforeSuite('Schedule Database tests', 'ScheduleDatabase');
        const config = new Configuration('./config-test.json');
        const dbconfig = config.getDatabaseTestConfig();
        dbconfig.database = "scheduledb_test";
        db = new ScheduleDBhelper(dbconfig);
    });
    beforeEach(async function () {
        try {
            await db.recreate();
        } catch (e) {
            console.log(e.message);
        }
        RPClientHelper.beforeTest(this.currentTest);
    });
    afterEach(function () {
        RPClientHelper.afterTest(
            this.currentTest,
            this.currentTest.err ? this.currentTest.err.stack : ''
        );
    });

    after(() => {
        RPClientHelper.afterSuite();
        db.endPool();
    });

    describe('#getSchedule', () => {
        it('should return a non-null object', () => {
            expect(db).to.not.be.null;
        });
    });

    describe('ScheduleDB#getScheduleInfo', () => {
        it('should return an empty schedule', async () => {
            const scheduleId = 0;
            const connection = await db.getConnection();
            const schedule = await db.getScheduleInfo(scheduleId, connection);
            connection.release();
            expect(schedule).to.be.null;
        });
    });

    describe('ScheduleDB#getScheduleInfo', () => {
        it('should return original schedule', async () => {
            const connection = await db.getConnection();
            const date = "2020-01-01";
            // have DB with pre-set data?
            const deptId = await db.addDepartment("department", connection);
            const scheduleId = await db.addSchedule(date, 14, deptId, true);
            const schedule = await db.getScheduleInfo(scheduleId, connection);
            connection.release();
            expect(deptId).to.be.eq(1);
            expect(scheduleId).to.be.eq(1);
            expect(schedule).to.not.be.empty;
            expect(schedule.dates).to.not.be.empty;
            expect(schedule.dates).to.deep.equal([1577854800, 1577941200, 1578027600, 1578114000, 1578200400, 1578286800, 1578373200, 1578459600, 1578546000, 1578632400, 1578718800, 1578805200, 1578891600, 1578978000]);
        });
    });

    describe('ScheduleDB#getSchedule', () => {
        it('should return original schedule', async () => {
            const connection = await db.getConnection();
            const date = "2020-01-01";
            // have DB with pre-set data?
            const deptId = await db.addDepartment("department", connection);
            const scheduleId = await db.addSchedule(date, 14, deptId, true);
            const schedule = await db.getSchedule(scheduleId);
            connection.release();
            expect(deptId).to.be.eq(1);
            expect(scheduleId).to.be.eq(1);
            expect(schedule).to.not.be.empty;
        });
    });
});
