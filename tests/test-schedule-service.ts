import { expect } from 'chai';
import { Configuration } from '../src/config/config';
import { Person, ScheduleLength } from '../src/models/Schedule';
import { Repeats, WorkPreference } from '../src/models/WorkPreference';
import { ScheduleService } from '../src/services/schedule.service';
import { RPClientHelper, ScheduleDBhelper } from './helper';

// TS_NODE_FILES env variable must be true in order to compile project typings

describe('ScheduleService', function () {
    let scheduleService: ScheduleService = null;
    let db: ScheduleDBhelper;
    before(() => {
        RPClientHelper.beforeSuite('Schedule Service tests', 'ScheduleService');
        const config = new Configuration('./config-test.json');
        const dbconfig = config.getDatabaseTestConfig();
        dbconfig.database = "scheduledb_test";
        db = new ScheduleDBhelper(dbconfig);
        scheduleService = new ScheduleService(db);
    });
    beforeEach(async function () {
        try {
            await db.recreate();
        } catch (e) {
            console.log(e.message);
        }
        RPClientHelper.beforeTest(this.currentTest);
    });
    afterEach(function () {
        RPClientHelper.afterTest(
            this.currentTest,
            this.currentTest.err ? this.currentTest.err.stack : ''
        );
    });

    after(() => {
        RPClientHelper.afterSuite();
        db.endPool();
    });

    describe('#', () => {
        it('should return a non-null object', () => {
            expect(db).to.not.be.null;
            expect(scheduleService).to.not.be.null;
        });
    });

    describe('ScheduleService#getWorkPreferences', () => {
        it('should return a non-null object', async () => {
            const person = <Person>{ name: "John Rogers", schedule_type: ScheduleLength.HOURS_8, work_week_hours: 40 };
            expect(await scheduleService.addPerson(person)).to.be.undefined;
            const workPreference = <WorkPreference>{ start: "2021-01-01", end: "2021-01-01", is_work: true, repeats: Repeats.wekly, person_id: person.id };
            workPreference.id = (await scheduleService.addWorkPreference(workPreference)).id;
            const result = await scheduleService.getWorkPreferences(workPreference.person_id);
            expect(person.id).to.be.eq(1);
            expect(result).to.have.length(1);
            expect(result[0]).to.deep.equal(workPreference);
        });
    });
});
