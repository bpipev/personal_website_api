import { expect } from 'chai';
import { DayScheduleType, Person, Schedule, ScheduleLength } from '../src/models/Schedule';
import { WorkPreference } from '../src/models/WorkPreference';
import { RPClientHelper } from './helper';

// TS_NODE_FILES env variable must be true in order to compile project typings

describe('Schedule', function () {
    before(() => {
        RPClientHelper.beforeSuite('Schedule class tests', 'Schedule');
    });
    beforeEach(async function () {
        RPClientHelper.beforeTest(this.currentTest);
    });
    afterEach(function () {
        RPClientHelper.afterTest(
            this.currentTest,
            this.currentTest.err ? this.currentTest.err.stack : ''
        );
    });

    after(() => {
        RPClientHelper.afterSuite();
    });

    describe('Schedule#constructor', () => {
        it('should initialize variables properly', async () => {
            const startDate = 1610230114;
            const duration = 3;
            const schedule_id = 1;
            const schedule = new Schedule(startDate, duration, schedule_id);
            expect(schedule.startDate).to.be.eq(startDate);
            expect(schedule.data).to.deep.equal({});
            expect(schedule.dates).to.deep.equal([1610230114, 1610316514, 1610402914]);
        });
    });

    describe('Schedule#addPerson~HOURS_8', () => {
        it('should return correct values when adding first person to schedule', async () => {
            const startDate = 1609711714;
            const duration = 7;
            const schedule_id = 1;
            const schedule = new Schedule(startDate, duration, schedule_id);
            const person_id = 1;
            const name = "John Doe";
            const person = <Person>{ name: name, schedule_type: ScheduleLength.HOURS_8, work_week_hours: 40, id: person_id };
            const weekendBeforeSchedule = {};
            const workPreference: WorkPreference[] = [];
            const result = schedule.addPerson(person, weekendBeforeSchedule, workPreference);
            // 5 days scheduled
            expect(result.length).to.eq(5);
            // Sunday 3-01-2021
            expect(result[0]).to.deep.equal([DayScheduleType.D, 1609711714]);
            // Tuesday 5-01-2021
            expect(result[4]).to.deep.equal([DayScheduleType.D, 1609884514]);
            // Wednesday 6-01-2021
            expect(result[3]).to.deep.equal([DayScheduleType.D, 1609970914]);
            // Thursday 7-01-2021
            expect(result[2]).to.deep.equal([DayScheduleType.D, 1610057314]);
            // Friday 8-01-2021
            expect(result[1]).to.deep.equal([DayScheduleType.D, 1610143714]);
            expect(schedule.data).to.not.be.null;
            expect(Object.keys(schedule.data)).to.deep.equal([String(person_id)]);
            // 5 days scheduled
            expect(Object.keys(schedule.data[person_id].workdays).length).to.eq(5);
            expect(schedule.data[person_id].workdays).to.deep.equal(
                {
                    "1609711714": DayScheduleType.D,
                    "1609884514": DayScheduleType.D,
                    "1609970914": DayScheduleType.D,
                    "1610057314": DayScheduleType.D,
                    "1610143714": DayScheduleType.D
                });
        });
    });

    describe('Schedule#addPerson~HOURS_12', () => {
        it('should return correct values when adding first person to schedule', async () => {
            const startDate = 1609711714;
            const duration = 14;
            const schedule_id = 1;
            const schedule = new Schedule(startDate, duration, schedule_id);
            const person_id = 1;
            const name = "John Doe";
            const person = <Person>{ name: name, schedule_type: ScheduleLength.HOURS_12, work_week_hours: 40, id: person_id };
            const weekendBeforeSchedule = {};
            const workPreference: WorkPreference[] = [];
            const result = schedule.addPerson(person, weekendBeforeSchedule, workPreference);
            // 7 days scheduled - 6 * 12 + 8 hours for two weeks
            expect(result.length).to.eq(7);
            // results ordered by priority per 2 weeks
            expect(result).to.deep.equal([
                [DayScheduleType.D, 1609711714], [DayScheduleType.J, 1610834914],
                [DayScheduleType.J, 1610143714], [DayScheduleType.J, 1610748514],
                [DayScheduleType.J, 1610057314], [DayScheduleType.J, 1610575714],
                [DayScheduleType.J, 1609884514]
            ]);
            expect(schedule.data).to.not.be.null;
            expect(Object.keys(schedule.data)).to.deep.equal([String(person_id)]);
            // 7 days scheduled
            expect(Object.keys(schedule.data[person_id].workdays).length).to.eq(7);
            expect(schedule.data[person_id].workdays).to.deep.equal(
                {
                    "1609711714": DayScheduleType.D,
                    "1610834914": DayScheduleType.J,
                    "1610143714": DayScheduleType.J,
                    "1610748514": DayScheduleType.J,
                    "1610057314": DayScheduleType.J,
                    "1610575714": DayScheduleType.J,
                    "1609884514": DayScheduleType.J
                });
        });
    });

    describe('Schedule#addPerson2~HOURS_8', () => {
        it('should return correct values when adding second person to schedule', async () => {
            const startDate = 1609711714;
            const duration = 7;
            const schedule_id = 1;
            const schedule = new Schedule(startDate, duration, schedule_id);
            const person_id = 1;
            const name = "John Doe";
            const person1 = <Person>{ name: name, schedule_type: ScheduleLength.HOURS_8, work_week_hours: 40, id: person_id };
            const name2 = "Jane Doe";
            const person_id_2 = 2;
            const person2 = <Person>{ name: name2, schedule_type: ScheduleLength.HOURS_8, work_week_hours: 40, id: person_id_2 };
            const weekendBeforeSchedule = {};
            const workPreference: WorkPreference[] = [];
            schedule.addPerson(person1, weekendBeforeSchedule, workPreference);
            const result = schedule.addPerson(person2, weekendBeforeSchedule, workPreference);
            // 5 days scheduled
            expect(result.length).to.eq(5);
            // Saturday 9-01-2021
            expect(result[0]).to.deep.equal([DayScheduleType.D, 1610230114]);
            // Monday 4-01-2021
            expect(result[1]).to.deep.equal([DayScheduleType.D, 1609798114]);
            // Friday 8-01-2021
            expect(result[2]).to.deep.equal([DayScheduleType.D, 1610143714]);
            // Thursday 7-01-2021
            expect(result[3]).to.deep.equal([DayScheduleType.D, 1610057314]);
            // Wednesday 6-01-2021
            expect(result[4]).to.deep.equal([DayScheduleType.D, 1609970914]);
            expect(schedule.data).to.not.be.null;
            expect(Object.keys(schedule.data)).to.deep.equal([String(person_id), String(person_id_2)]);
            // 5 days scheduled
            expect(Object.keys(schedule.data[person_id].workdays).length).to.eq(5);
            expect(schedule.data[person_id_2].workdays).to.deep.equal(
                {
                    "1610230114": DayScheduleType.D,
                    "1610143714": DayScheduleType.D,
                    "1609970914": DayScheduleType.D,
                    "1610057314": DayScheduleType.D,
                    "1609798114": DayScheduleType.D
                });
        });
    });

    describe('Schedule#addExistingPerson', () => {
        it('should add person data to schedule', async () => {
            const startDate = 1609711714;
            const duration = 7;
            const schedule_id = 1;
            const schedule = new Schedule(startDate, duration, schedule_id);
            const person_id = 1;
            const name = "John Doe";
            schedule.addExistingScheduledDay(name, startDate, DayScheduleType.D, person_id);
            expect(schedule.data).to.not.be.null;
            expect(Object.keys(schedule.data)).to.deep.equal([String(person_id)]);
            expect(Object.keys(schedule.data[person_id].workdays).length).to.eq(1);
            expect(schedule.data[person_id].workdays).to.deep.equal(
                {
                    "1609711714": DayScheduleType.D
                });
        });
    });
});
